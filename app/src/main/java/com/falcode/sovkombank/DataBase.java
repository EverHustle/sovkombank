package com.falcode.sovkombank;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by CeoFalcode on 10.03.2017.
 */

class DataBase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "hello_world_DB";
    private static final int DATABASE_VERSION = 1;
    static final String TABLE_NAME = "hello_world";
    static final String COLUMN_NAME = "parsed_text";
    private static final String KEY_ID = "_id";

    DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME + "(" + KEY_ID + " integer primary key," + COLUMN_NAME + " text" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

package com.falcode.sovkombank;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //MainActivity fields
    Parser parser = new Parser();
    Context context;
    final int REQUEST_PERIOD = 43200000; //12 hours period im millis

    //DateBase related fields
    Storage storage = new Storage();
    private DataBase db;
    SQLiteDatabase database;
    ContentValues cv = new ContentValues();

    //UI related fields
    TextView publishTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        boolean otherRun = storage.checkKey(context); //False if run app for the first time. True else.
        if (!otherRun) {
            firstRun(context);
        } else {
            secondRun(context);
        }


    }

    //Invoke parser from Parser, takes data from remote host and save it to dateBase.
    private void firstRun(Context context) {
        parser.threadParsing.start();
        try {
            parser.threadParsing.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String parsedText = parser.getResult(); //get parsed text from parser
        writeToDataBase(parsedText, context); //write parsed text to DB
        String text = readDataBase(context); //read recently written text from DB
        setTextToTextView(text); //set read text to text view
        startAlarm();
    }

    //Gets text from DB and sets it to text view. Then starts service.
    public void secondRun(Context context) {
        String text = readDataBase(context); //read recently updated text from DB
        setTextToTextView(text); //set read text to text view
        startAlarm();
    }

    //Run by service. Parse remote address, update text from DB to parsed.
    public void serviceRun(Context context) {
        parser.threadParsing.start();
        try {
            parser.threadParsing.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String parsedText = parser.getResult(); //get parsed text from parser
        updateDataBase(parsedText, context); //update text in database
    }

    //Write parsed text to DB
    void writeToDataBase(String parsed_text, Context context) {
        db = new DataBase(context);
        database = db.getWritableDatabase();
        cv.put(DataBase.COLUMN_NAME, parsed_text);
        database.insert(DataBase.TABLE_NAME, null, cv);
    }

    //Update text in DB to parsed
    private void updateDataBase(String text, Context context) {
        db = new DataBase(context);
        String oldText = readDataBase(context);
        cv.put(DataBase.COLUMN_NAME, text);
        database.update(DataBase.TABLE_NAME, cv, DataBase.COLUMN_NAME + "= ?", new String[]{oldText});
        database.close();
    }

    //Read data from database and send them back to invoker.
    private String readDataBase(Context context) {
        String[] columns = {DataBase.COLUMN_NAME};
        db = new DataBase(context);
        database = db.getWritableDatabase();
        Cursor cursor = database.query(DataBase.TABLE_NAME, columns, null, null, null, null, null);
        cursor.moveToNext();
        int textIndex = cursor.getColumnIndex(DataBase.COLUMN_NAME);
        String text = cursor.getString(textIndex);
        cursor.close();
        return (text);
    }

    //Show text from database in publishTV.
    private void setTextToTextView(String text) {
        publishTV = (TextView) findViewById(R.id.publishTV);
        publishTV.setText(text);
    }

    //Start service
    public void startAlarm() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis(); // time when alarm starts. If precise time needed, use Calendar.
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                REQUEST_PERIOD, pIntent);
    }


    //Use this method if you want to stop alarm. Might need context.
    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }
}
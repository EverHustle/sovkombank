package com.falcode.sovkombank;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class ParseIntentService extends IntentService {
    MainActivity main = new MainActivity();
    Context context;

    public ParseIntentService() {
        super("ParseIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        main.serviceRun(this);
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }
}
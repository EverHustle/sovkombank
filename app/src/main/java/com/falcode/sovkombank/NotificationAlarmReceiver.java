package com.falcode.sovkombank;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationAlarmReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 10000;
    public static final String ACTION = "com.falcode.gratsme";

    public NotificationAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, ParseIntentService.class);
        context.startService(i);
    }
}

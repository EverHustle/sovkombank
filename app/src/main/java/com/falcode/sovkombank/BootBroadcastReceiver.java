package com.falcode.sovkombank;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Created by CeoFalcode on 13.02.2017.
 */

public class BootBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, ParseIntentService.class);
        startWakefulService(context, startServiceIntent);
    }

}

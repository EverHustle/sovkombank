package com.falcode.sovkombank;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by CeoFalcode on 10.03.2017.
 */

class Storage {
    private SharedPreferences spref;
    private final String KEY = "SECOND_RUN";

    /**
     * @param context
     * @return firstRun
     */
    //Check if this is first run. If firstRun -> call saveKey, init DB. Else update info in DB.
    boolean checkKey(Context context) {
        spref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        boolean firstRun = spref.getBoolean(KEY, false);
        if (!firstRun) {
            saveKey(true, context);
            return false;
        }
        return true;
    }

    /**
     * @param firstRun
     * @param context
     */
    //Saves key to SharedPreferences on first run and updates on second.
    private void saveKey(boolean firstRun, Context context) {
        spref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();
        editor.putBoolean(KEY, firstRun);
        editor.apply();
    }
}

package com.falcode.sovkombank;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by CeoFalcode on 10.03.2017.
 */

class Parser {
    volatile private String result;
    final String ADDRESS = "https://ru.wikipedia.org/wiki/Hello,_world!"; //Parsable site address
    final String TEG = ".mw-content-ltr"; //Div class from page that we parse

    String getResult() {
        return result;
    }

    Thread threadParsing = new Thread(new Runnable() {
        @Override
        synchronized public void run() {
            try {
                Document doc = Jsoup.connect(ADDRESS).get();
                Elements element = doc.select(TEG);
                result = element.text();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    });
}